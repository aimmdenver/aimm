Using the latest facial recognition and a fully conversational design, AIMM is the future of matchmaking. AIMM is easy to use by asking you a series of questions to determine your best match!

Address: 2001 Lincoln St, #1910, Denver, CO 80202, USA

Phone: 720-261-4458
